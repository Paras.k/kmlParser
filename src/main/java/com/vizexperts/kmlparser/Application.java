package com.vizexperts.kmlparser;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import com.vizexperts.kmlparser.types.PointAndLineString;
import com.vizexperts.kmlparser.types.TrackTimeAndData;
import de.micromata.opengis.kml.v_2_2_0.*;
import de.micromata.opengis.kml.v_2_2_0.gx.Track;


public class Application {

    private static List<PointAndLineString> pointAndLineStrings = new ArrayList<PointAndLineString>();
    private static List<TrackTimeAndData> trackTimeAndData = new ArrayList<TrackTimeAndData>();
    private static final String GPS_LOG = "./GPS_LOG_OF_SOG-31.csv";
    private static final String KPT_OPS = "./KPT_OPS_PLAN_DT-02-03.08.17.csv";

    public void parseKml() throws IOException {
        String src = "KPT OPS PLAN DT-02-03.08.17 SOG-03, 31 (ONLY 31 GPS DONE, 03 NOT SENT HIS GPS LOG).kml";
        InputStream is = getClass().getClassLoader().getResourceAsStream(src);
        if(is.available()!=0) {
            Kml kml = Kml.unmarshal(is);
            Feature feature = kml.getFeature();
            parseFeature(feature);
        }
    }

    private void parseFeature(Feature feature) {
        if(feature != null) {
            if(feature instanceof Document) {
                Document document = (Document) feature;
                List<Feature> featureList = document.getFeature();
                for(Feature documentFeature : featureList) {
                    if(documentFeature instanceof Folder) {
                        Folder folder = (Folder) documentFeature;
                        List<Feature> featureList1 = folder.getFeature();
                        for(Feature documentFeature1 : featureList1){
                            if(documentFeature1 instanceof Placemark){
                                Placemark placemark = (Placemark) documentFeature1;
                                Geometry geometry = placemark.getGeometry();
                                String name = placemark.getName();
                                parseGeometry(geometry,name);
                            }
                        }

                    }
                }
            }
        }
    }

    private void parseGeometry(Geometry geometry , String name) {
        if(geometry != null) {

            if(geometry instanceof Point) {
                Point point = (Point) geometry;
                List<Coordinate> coordinatesList = point.getCoordinates();
                if (coordinatesList != null) {
                    for (Coordinate coordinate : coordinatesList) {
                        parseCoordinateOfPoint(coordinate,name);
                    }
                }
            }else{
                if(geometry instanceof LineString){
                    LineString lineString = (LineString) geometry;
                    List<Coordinate> coordinateList1 = ((LineString) geometry).getCoordinates();
                    if(coordinateList1 != null){
                        for(Coordinate coordinate1 : coordinateList1){
                            parseCoordinateOfLineString(coordinate1,name);
                        }
                    }
                }else{
                    if(geometry instanceof Track){
                        Track track = (Track) geometry;
                        List<String> whenList = ((Track) geometry).getWhen();
                        List<String> coordList = ((Track) geometry).getCoord();
                        if(whenList != null && coordList != null) {
                            parseCoordinateOfTrack(whenList,coordList,name);
                        }
                    }
                }
            }
        }
        }

    private void parseCoordinateOfPoint(Coordinate coordinate , String name) {
        if(coordinate != null) {
            PointAndLineString pointObj = new PointAndLineString();
            pointObj.setType("Point");
            pointObj.setLongitude(coordinate.getLongitude());
            pointObj.setLatitude(coordinate.getLatitude());
            pointObj.setAltitude(coordinate.getAltitude());
            pointObj.setName(name);
            pointAndLineStrings.add(pointObj);
        }
    }

    private void parseCoordinateOfLineString(Coordinate coordinate , String name) {
        if(coordinate != null) {
            PointAndLineString lineStringObj = new PointAndLineString();
            lineStringObj.setType("LineString");
            lineStringObj.setLongitude(coordinate.getLongitude());
            lineStringObj.setLatitude(coordinate.getLatitude());
            lineStringObj.setName(name);
            pointAndLineStrings.add(lineStringObj);
        }
    }

    private void parseCoordinateOfTrack(List<String> whenList , List<String> coordList , String name) {
        for(int i=0;i<whenList.size();i++){
            String temp[] = coordList.get(i).split(" ");
            TrackTimeAndData trackObj =new TrackTimeAndData();
            trackObj.setType("Track");
            trackObj.setLongitude(Double.parseDouble(temp[0]));
            trackObj.setLatitude(Double.parseDouble(temp[1]));
            trackObj.setAltitude(Double.parseDouble((temp[2])));
            trackObj.setWhen(whenList.get(i));
            trackObj.setDate(name);
            trackTimeAndData.add(trackObj);
        }

    }

    public void writePointAndLineStringFile(){
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(KPT_OPS));
             CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                     .withHeader("Type","Longitude","Latitude","Altitude","Name"));
        ){
            for(PointAndLineString pointAndLineString1 : pointAndLineStrings){
                csvPrinter.printRecord(pointAndLineString1.getType(),Double.toString(pointAndLineString1.getLongitude()),Double.toString(pointAndLineString1.getLatitude()),Double.toString(pointAndLineString1.getAltitude()),pointAndLineString1.getName());
            }
            csvPrinter.flush();
        }catch (Exception e){
            System.out.println(e);
        }
    }

    public void writeTrackFile(){
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(GPS_LOG));
             CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                     .withHeader("Type","Longitude","Latitude","Altitude","When","Date"));
             ){
            for(TrackTimeAndData trackTimeAndData : trackTimeAndData){
                csvPrinter.printRecord(trackTimeAndData.getType(),Double.toString(trackTimeAndData.getLongitude()),Double.toString(trackTimeAndData.getLatitude()),Double.toString(trackTimeAndData.getAltitude()),trackTimeAndData.getWhen(),trackTimeAndData.getDate());
            }
            csvPrinter.flush();
        }catch (Exception e){
            System.out.println(e);
        }
    }

    public static void main(String[] args) throws IOException {
       Application obj= new Application();
       obj.parseKml();
       obj.writePointAndLineStringFile();
       obj.writeTrackFile();
    }
}



