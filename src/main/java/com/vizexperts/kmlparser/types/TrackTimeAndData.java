package com.vizexperts.kmlparser.types;

public class TrackTimeAndData {

    String type;
    double latitude;
    double longitude;
    double altitude;
    String when;
    String date;

    public void setDate(String name) {
        this.date = name;
    }

    public String getDate() {
        return date;
    }


    public String getType() {
        return type;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public String getWhen() {
        return when;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public void setWhen(String when) {
        this.when = when;
    }
}

